# encoding: utf-8

from app import db
from app.model.model import Model


class KeyModel(db.Model, Model):
    __tablename__ = "key"
    id = db.Column(db.Integer, primary_key=True)
    # Public key to use for public scripts
    public = db.Column(db.String(40))
    # Private key to use for requesting solutions
    secret = db.Column(db.String(40))
    # Owner of the key
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("UserModel", backref=db.backref("keys", lazy="dynamic"))

    @property
    def public_secret(self):
        return self.secret[:2] + "..." + self.secret[-2:]
