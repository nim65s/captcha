# encoding: utf-8

from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy


api = Api()
db = SQLAlchemy()
migrate = Migrate()
