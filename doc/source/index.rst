.. LibreCaptcha documentation master file, created by
   sphinx-quickstart on Wed Sep 30 14:44:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: title.png

Bienvenue sur la documentation de LibreCaptcha.

LibreCaptcha est un projet visant à fournir des [Captcha]()
les plus accessibles possibles ainsi qu'à protéger la vie privée de ses
utilisateurs.

Pour cela, vous pouvez vous rendre sur https://LibreCaptcha.com/ et vous
inscrire. Vous pourrez alors lister les domaines pour lesquels vous souhaitez
utiliser LibreCaptcha et générer des clefs pour chacun d'eux. Une fois les clefs
obtenues, il ne vous restera plus qu'à intégrer LibreCaptcha dans votre site web
pour le protéger des robots malveillants.


Intégrer LibreCaptcha sur son site web
--------------------------------------

L'intégration se fait en trois étapes. Tout d'abord, il faut appeler un fichier
javascript depuis la page qui contient le formulaire à protéger :

.. code-block:: html

    <script src="https://LibreCaptcha.com/LibreCaptcha.js" async defer></script>

Il faut également ajouter un code html simple pour y faire apparaitre
LibreCaptcha, dans le formulaire lui-même cette fois-ci. Il est nécessaire de
spécifier votre clef publique ici :

.. code-block:: html

    <div class="LibreCaptcha" data-sitekey="CLEF_PUBLIQUE"></div>

Enfin, du côté du code qui valide le formulaire, il va falloir faire appel à
l'API en fournissant votre clef secrète et la réponse fournie par l'utilisateur.
Cela peut donner, en python :

.. code-block:: python

    captcha_result = requests.post(
        "http://LibreCaptcha.com/verifychallenge,
        data={
            "LibreCaptcha-challenge": request.form.get("LibreCaptcha-challenge", ""),
            "LibreCaptcha-answer": request.form.get("LibreCaptcha-answer", ""),
            "private-key": CLEF_PRIVEE,
        }
    ).json()
    success = captcha_result["success"]

Et voilà ! Désormais le formulaire vérifiera d'abord la cohérence de la réponse
fournie à LibreCaptcha avant d'effectuer sa propre action.

reCaptcha
~~~~~~~~~

Si vous avez déjà reCaptcha sur votre site, il est très simple de passer à
LibreCaptcha. Il suffit de remplacer :

    * le lien javascipt du reCaptcha en mettant l'url de LibreCaptcha
    * les clefs publique et secrète reCaptcha par celles de LibreCaptcha
    * l'appel à l'API reCaptcha par celle de LibreCaptcha

.. code-block:: python

    captcha_result = requests.post(
        "https://LibreCaptcha.com/recaptcha/api/siteverify",
        data={
            "response": request.form.get("g-recaptcha-response", ""),
            "secret": CLEF_PRIVEE,
        }
    ).json()
    success = captcha_result["success"]

Le fonctionnement de LibReCatpcha est adapté afin de vous simplifier la vie !



.. toctree::
   :maxdepth: 2
   :caption: Liens:

   installation
   contribuer


Index et tables
===============

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
