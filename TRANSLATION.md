# Translation

use Flask-Babel for translations

## Extraction of strings to translate

    pybabel extract -F app/babel.cfg -o locale/messages.pot ./


## Initialisation of a specific language

    pybabel init -i locale/messages.pot -d locale -l fr


## Update of strings for all translated languages

    pybabel update -i locale/messages.pot -d locale


## Compile each languages to use them

    pybabel compile -d locale
