# Installation

In order to run LibreCaptcha, you need ffmpeg, python3 and a mysql server (Debian packages are ffmpeg, default-mysql-server, default-libmysqlclient-dev, python3-dev, build-essential).

First of all, clone the repo :

    git clone https://framagit.org/Mindiell/captcha.git

Then go into its folder and create a virtual environment with python3

    cd captcha
    virtualenv -p python3 ve

Next, active virtual environment and install requirements needed

    source ve/bin/activate
    pip install -r requirements

Create your database

    CREATE DATABASE IF NOT EXISTS `librecaptcha` DEFAULT CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;
    CREATE USER 'librecaptcha'@'localhost' IDENTIFIED BY 'librecaptcha';
    GRANT ALL PRIVILEGES ON librecaptcha.* TO 'librecaptcha'@'localhost';

Copy the default config.py and set up your own configuration

    cp default_config.py config.py
    vi config.py (or emacs or nano or whatever you prefer)

After creating the necessary database (and set your config file as needed), initialize it

    export FLASK_APP=server.py
    flask db upgrade

Then generate challenges (first time it can take a lot of time)

    flask captcha generate

Finally, you can run the server (directly or by using a service like supervisor for example)

    python server.py


# Helping

Help is more than welcome, and you can help by doing a lot of different stuff, such as :

* Code (obviously)
* Design
* UX
* Test (either by writing some or running some)
* Documentation
* Translation (website and/or application)

## How to help

In order to help, you can contact me (through gitlab I think). At this time, project is
kind of in prototyping stage and there is a lot to do in order to make it completely usable
in production.


# Friends

[Meta-Press.es](https://framagit.org/Siltaar/meta-press-ext) : decentralized press meta-search engine.
[Caliopen](https://github.com/CaliOpen/Caliopen) : The best way to discuss with your contacts, while preserving your privacy.
