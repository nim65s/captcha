# encoding: utf-8
"""
This module is used in order to create challenges
"""

import os
import random
import time

import click
import requests
from bs4 import BeautifulSoup
from PIL import Image, ImageFilter

import config
from app.model.visual_challenge import VisualChallengeModel
from app.model.visual_source import VisualSourceModel
from command.captcha.utils import ACCEPTED_LICENSES, normalize_license

WIKIMEDIA_URL = "https://commons.wikimedia.org/wiki/Special:Random/File"

SLEEP_TIME = 2

VISUAL_SOURCE_SIZE = 280
VISUAL_CHALLENGE_SIZE = 90


def get_picture():
    try:
        # No need to spam Wikimedia Foundation
        time.sleep(SLEEP_TIME)
        # Let"s find some new source on Wikimedia Commons
        click.echo(".", nl=False)
        new_source = requests.get(WIKIMEDIA_URL, headers=config.USER_AGENT)
        if new_source.url.lower()[-4:] in (".jpg", "jpeg", ".jpe"):
            # Source is a picture (or sort of) let"s parse its metadata
            html = BeautifulSoup(new_source.text, "html.parser")
            picture = {}
            try:
                picture["file_url"] = html.find("a", class_="internal")["href"].strip()
                picture["licenses"] = html.find_all("span", class_="licensetpl_short")
                picture["author"] = html.find(
                    "td", id="fileinfotpl_aut"
                ).next_sibling.next_sibling.text.strip()
            except Exception as e:
                # If an error occurs, do nothing special apart logging it
                click.echo(new_source.url)
                click.echo("    => %s" % str(e))
                # And let"s try another media
                return None
            # License should be ok with re-use
            for picture_license in picture["licenses"]:
                for license in ACCEPTED_LICENSES:
                    if normalize_license(
                        picture_license.text.strip().lower()
                    ).startswith(license):
                        picture["license"] = normalize_license(
                            picture_license.text.strip().lower()
                        )
                        return picture
    except Exception as e:
        # If an error occurs, do nothing special apart logging it
        click.echo(new_source.url)
        click.echo("    => %s" % str(e))
    # No media found
    return None


def get_number_of_active_challenges():
    return VisualChallengeModel.query.filter_by(inactive=False).count()


def get_inactive_challenges():
    return VisualChallengeModel.query.filter_by(inactive=True).all()


def get_all_challenges():
    return VisualChallengeModel.query.all()


def create_source(picture):
    source = {}
    # Generate an id for this picture
    picture_id = "%064x" % random.getrandbits(256)
    # and set its path
    picture_path = os.path.join(
        config.VISUAL_SOURCE_DIR,
        "%s/%s/%s/" % (picture_id[:2], picture_id[2:4], picture_id[4:6]),
    )
    # Create directory if necessary
    os.makedirs(picture_path)
    # Download the picture itself
    result = requests.get(picture["file_url"], stream=True, headers=config.USER_AGENT)
    # And save it on filesystem
    with open("%s%s.jpg" % (picture_path, picture_id), "wb") as f:
        for chunk in result.iter_content(chunk_size=4096):
            f.write(chunk)
    source["original"] = Image.open("%s%s.jpg" % (picture_path, picture_id))
    # Test if picture is wide enough
    if (
        source["original"].size[0] + source["original"].size[1]
        <= 2 * VISUAL_SOURCE_SIZE
    ):
        # Nope, destroy it
        os.remove("%s%s.jpg" % (picture_path, picture_id))
        return
    else:
        # Picture seems ok, resize it
        source["original"].thumbnail(
            (VISUAL_SOURCE_SIZE, VISUAL_SOURCE_SIZE), Image.ANTIALIAS
        )
        source["original"].save("%s%s.jpg" % (picture_path, picture_id), "JPEG")
        # And add its metadata to database
        source["source"] = VisualSourceModel(
            filename="%s.jpg" % picture_id,
            # Sometimes, author is multilined
            author=picture["author"].split("\n")[0],
            license=picture["license"],
        )
        source["source"].save()
    return source


def get_sources():
    return VisualSourceModel.query.all()


def get_source_image(image_path):
    return Image.open("%s" % image_path)


def create_challenge(source_id, original):
    try:
        # Each challenge is a 120x120 picture blured at random
        token = "%064x".upper() % random.getrandbits(256)
        pos_x = random.randint(0, original.size[0] - VISUAL_CHALLENGE_SIZE)
        pos_y = random.randint(0, original.size[1] - VISUAL_CHALLENGE_SIZE)
        challenge_path = os.path.join(
            config.VISUAL_CHALLENGE_DIR,
            "%s/%s/%s/" % (token[:2], token[2:4], token[4:6]),
        )
        os.makedirs(challenge_path)
        # Do the crop, the blur, and save it on disk
        original.crop(
            (pos_x, pos_y, pos_x + VISUAL_CHALLENGE_SIZE, pos_y + VISUAL_CHALLENGE_SIZE)
        ).filter(ImageFilter.BLUR).save("%s%s.jpg" % (challenge_path, token))
        # Then save it in database
        VisualChallengeModel(
            visual_source_id=source_id,
            inactive=False,
            token=token,
        ).save()
        return True
    except:
        return False


def delete_challenge(challenge):
    challenge_filepath = challenge.filepath
    challenge_path_1 = os.path.join(
        config.VISUAL_CHALLENGE_DIR,
        "%s/" % challenge.token[:2],
    )
    challenge_path_2 = os.path.join(challenge_path_1, "%s/" % challenge.token[2:4])
    challenge_path_3 = os.path.join(challenge_path_2, "%s/" % challenge.token[4:6])
    source = challenge.visual_source
    # Deleting challenge
    challenge.delete()
    # Deleting file
    try:
        os.remove(challenge_filepath)
    except Exception as e:
        # If an error occurs, do nothing special apart logging it
        click.echo("    => %s" % str(e))
    # Verifying folders
    if os.listdir(challenge_path_3) == []:
        # Empty folder, remove it
        try:
            os.rmdir(challenge_path_3)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
    if os.listdir(challenge_path_2) == []:
        # Empty folder, remove it
        try:
            os.rmdir(challenge_path_2)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
    if os.listdir(challenge_path_1) == []:
        # Empty folder, remove it
        try:
            os.rmdir(challenge_path_1)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
    if VisualChallengeModel.query.filter_by(visual_source_id=source.id).count() == 0:
        # No more challenge for this source, delete it
        delete_source(source)


def delete_source(source):
    source_filepath = source.filepath
    source_path_1 = os.path.join(config.VISUAL_SOURCE_DIR, "%s/" % source.filename[:2])
    source_path_2 = os.path.join(source_path_1, "%s/" % source.filename[2:4])
    source_path_3 = os.path.join(source_path_2, "%s/" % source.filename[4:6])
    # Deleting source
    source.delete()
    # Deleting file
    try:
        os.remove(source_filepath)
    except Exception as e:
        # If an error occurs, do nothing special apart logging it
        click.echo("    => %s" % str(e))
    # Verifying folders
    if os.listdir(source_path_3) == []:
        # Empty folder, remove it
        try:
            os.rmdir(source_path_3)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
    if os.listdir(source_path_2) == []:
        # Empty folder, remove it
        try:
            os.rmdir(source_path_2)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
    if os.listdir(source_path_1) == []:
        # Empty folder, remove it
        try:
            os.rmdir(source_path_1)
        except Exception as e:
            # If an error occurs, do nothing special apart logging it
            click.echo("    => %s" % str(e))
